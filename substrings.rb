
def find_substrings(str, dict)
  split_string = str.split(" ")
  puts "\n"
  matched_substrings= Hash.new 
  dict.each do |word|
    split_string.each do |splt|
      check_match = splt.match?(/#{word}/i)
      puts "Checking if {#{word}} is in {#{splt}}...#{check_match}"
      if check_match == true
        matched_substrings.has_key?(word) ? matched_substrings[word] += 1 : matched_substrings[word] = 1
      end
    end 
    puts "\n"
  end
  return matched_substrings
end


print "Enter a string: "
usr_string = gets.chomp

dictionary = ["below","down","go","going","horn","how","howdy","it","i","low","own","part","partner","sit"]
puts "\n#{find_substrings(usr_string,dictionary)}"